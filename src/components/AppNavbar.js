import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useState, Fragment, useContext } from 'react';
import UserContext from '../UserContext';

export default function AppNavbar(){
	const { user } = useContext(UserContext);
	console.log(user);

	//const [userId, setUserId] = useState(localStorage.getItem("userId"));

	return(
		<Navbar bg="light" expand="lg">
			<Container fluid>
		    <Navbar.Brand as={Link} to="/">Coffee Break</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
				    <Nav.Link as={NavLink} to="/">Home</Nav.Link>
				    
				    <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

				    {(user.id === null)
						?
						<Fragment>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</Fragment>
						:
						<Fragment>
							{(user.isAdmin)
							?
							<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							:
				    	<Fragment>
				    		<Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
				    		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
				    	</Fragment>
				    	}
						</Fragment>
						}

				  </Nav>
		    </Navbar.Collapse>
			</Container>
		</Navbar>	
	)
}