import { Card, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';

export default function OrderProductCard({orderProductProp}) {

  const {productId, quantity} = orderProductProp;
  const [prodName, setProdName] = useState("");

  const getProdName = (productId) => {
    fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
    .then(res => res.json())
    .then(data => {
      setProdName(data.name);
    });
  };

  getProdName(productId);

  return (
    <Col md={{span: 6, offset: 3}}>
      <Card className="mt-2 mb-2">
        <Card.Body>
          <Card.Subtitle>Product Name: {prodName}</Card.Subtitle>
          <Card.Text>Quantity: {quantity}</Card.Text>
        </Card.Body>
      </Card>
    </Col>
  )
}