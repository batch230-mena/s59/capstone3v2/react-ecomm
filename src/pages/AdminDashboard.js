import {useContext, useEffect, useState} from "react";
import { Col, Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import LoggedInBanner from '../components/LoggedInBanner';
import OrderCard from '../components/OrderCard';

export default function AdminDashboard(){
	//const { user } = useContext(UserContext);
	const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
	const [userRole, setUserRole] = useState(localStorage.getItem("userRole"));

	// Create all* states
	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);
	const [allOrders, setAllOrders] = useState([]);

	// State hooks
	const [productId, setProductId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
  const [stocks, setStocks] = useState(0);

  // State for submit button
  const [isActive, setIsActive] = useState(false);

  // State for Modals
  const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);
	const [showAdmin, setShowAdmin] = useState(false);
	const [showOrders, setShowOrders] = useState(false);

	// To control the add product modal pop out
	const openAdd = () => setShowAdd(true); 
	const closeAdd = () => setShowAdd(false); 

	// To control the edit product modal pop out
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific product
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		});

		setShowEdit(true);
	};

	const closeEdit = () => {
    setName('');
    setDescription('');
    setPrice(0);
    setStocks(0);

		setShowEdit(false);
	};

	// To control the set role modal pop out
	const openAdmin = () => {
		fetchUsers();
		setShowAdmin(true);
	}
	
	const closeAdmin = () => setShowAdmin(false);

	// To control the show orders modal pop out
	const openOrders = () => {
		fetchOrders();
		setShowOrders(true);
	}

	const closeOrders = () => setShowOrders(false);

	// View all users (admin and non-admin)
	const fetchUsers = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return (
					<tr key={user._id}>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.isAdmin ? "Admin" : "Non-admin"}</td>
						<td>
							{
								(user.isAdmin)
								?
								<>
									<Col className="p-1 text-center">
										<Button variant="danger" size="sm" className="mx-1" onClick={() => setAdmin(user._id, false)}>Set as Non-admin</Button>
									</Col>
								</>
								:
								<>
									<Col className="p-1 text-center">
										<Button variant="success" size="sm" className="mx-1" onClick={() => setAdmin(user._id, true)}>Set as Admin</Button>
									</Col>
								</>
							}
						</td>
					</tr>
				)
			}));

		});

	}

	// to fetch all users in the first render of the modal.
	useEffect(() => {
		fetchUsers();
	}, [])

	// View all products (active & inactive)
	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								(product.isActive)
								?
								<>
									<Col className="p-1 text-center">
										<Button variant="danger" size="sm" className="mx-1" onClick={() => archive(product._id, product.name)}>Deactivate</Button>
									</Col>
									<Col className="p-1 text-center">
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</Col>
								</>
								:
								<>
									<Col className="p-1 text-center">
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Activate</Button>
									</Col>
									<Col className="p-1 text-center">
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</Col>
								</>
							}
						</td>
					</tr>
				)
			}));
		});
	}

	// to fetch all products in the first render of the page.
	useEffect(() => {
		fetchData();
	}, [])

	// View all orders (paid and pending)
	const fetchOrders = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/orders/orders/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log("fetchOrders");
			console.log(data);

			setAllOrders(data.map((order, orderId) => {
				return (
					<OrderCard key={orderId} orderProp={order} />
				)
			}));
		});
	}

	// to fetch all orders in the first render of the modal.
	useEffect(() => {
		fetchOrders();
	}, [])

	// Set product to inactive
	const archive = (id, productName) => {
		console.log(id);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Deactivate successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});

				fetchData();
			}
			else{
				Swal.fire({
					title: "Deactivate error",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Set product to active
	const unarchive = (id, productName) => {
		console.log(id);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Activate successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				
				fetchData();
			}
			else{
				Swal.fire({
					title: "Activate error",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Create new product
	const addProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
    	method: "POST",
    	headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
		    name: name,
		    description: description,
		    price: price,
		    stocks: stocks
			})
    })
    .then(res => res.json())
    .then(data => {
    	console.log(data);

    	if(data){
    		Swal.fire({
    		    title: "Product successfully created",
    		    icon: "success",
    		    text: `${name} is now created`
    		});
    		fetchData();
    		closeAdd();
    	}
    	else{
    		Swal.fire({
    		    title: "Product creation error",
    		    icon: "error",
    		    text: `Something went wrong. Please try again later!`
    		});
    		closeAdd();
    	}
    })

    setName('');
    setDescription('');
    setPrice(0);
    setStocks(0);
	}

	// Edit specific product info
	const editProduct = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
    	method: "PUT",
    	headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
		    name: name,
		    description: description,
		    price: price,
		    stocks: stocks
		})
    })
    .then(res => res.json())
    .then(data => {
    	console.log(data);

    	if(data){
    		Swal.fire({
    		    title: "Product successfully updated",
    		    icon: "success",
    		    text: `${name} is now updated`
    		});

    		fetchData();
    		closeEdit();
    	}
    	else{
    		Swal.fire({
    		    title: "Product update error",
    		    icon: "error",
    		    text: `Something went wrong. Please try again later!`
    		});

    		closeEdit();
    	}
    })

    setName('');
    setDescription('');
    setPrice(0);
    setStocks(0);
	} 

	// Set role of user
	const setAdmin = (id, isAdmin) => {
		console.log(id);
		console.log(isAdmin);

		fetch(`${process.env.REACT_APP_API_URL}/users/${id}/admin`, {
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAdmin: isAdmin
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Change role successful",
					icon: "success",
					text: "Role is now changed."
				});

				fetchUsers();
			}
			else{
				Swal.fire({
					title: "Change role error",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Submit button validation for add/edit product
	useEffect(() => {
    if(name !== "" && description !== "" && price > 0 && stocks > 0){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
  }, [name, description, price, stocks]);

	return(
		// (user.isAdmin)
		(userRole)
		?
		<>
			<LoggedInBanner firstName={firstName} />
			{/*Header for the admin dashboard*/}
			<div className="mt-3 mb-3 text-center">
				<h3>Admin Dashboard</h3>
				<Button variant="success" className="mx-2" onClick={openAdd}>Create product</Button>
				<Button variant="warning" className="mx-2" onClick={openOrders}>Show orders</Button>
				<Button variant="danger" className="mx-2" onClick={openAdmin}>Set user role</Button>
			</div>

			{/*For view all products in the database.*/}
			<Table striped bordered hover>
	      <thead>
	        <tr>
	          <th>Product ID</th>
	          <th>Product Name</th>
	          <th>Description</th>
	          <th>Price</th>
	          <th>Stocks</th>
	          <th>Status</th>
	          <th>Actions</th>
	        </tr>
	      </thead>
	      <tbody>
        	{allProducts}
	      </tbody>
	    </Table>

	    {/*Modal for adding a new product*/}
      <Modal show={showAdd} fullscreen={false} onHide={closeAdd}>
    		<Form onSubmit={e => addProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Create Product</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
	        	<Form.Group controlId="name" className="mb-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter Product Name" 
                value = {name}
                onChange={e => setName(e.target.value)}
                required />
            </Form.Group>

	          <Form.Group controlId="description" className="mb-3">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
              	as="textarea"
              	rows={3}
                placeholder="Enter Product Description" 
                value = {description}
                onChange={e => setDescription(e.target.value)}
                required />
	          </Form.Group>

            <Form.Group controlId="price" className="mb-3">
              <Form.Label>Product Price</Form.Label>
              <Form.Control 
                type="number" 
                min="0"
                placeholder="Enter Product Price" 
                value = {price}
                onChange={e => setPrice(e.target.value)}
                required />
            </Form.Group>

            <Form.Group controlId="stocks" className="mb-3">
              <Form.Label>Product Stocks</Form.Label>
              <Form.Control 
                type="number" 
                min="0"
                placeholder="Enter Product Stocks" 
                value = {stocks}
                onChange={e => setStocks(e.target.value)}
                required />
            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				  : 
  				    <Button variant="danger" type="submit" id="submitBtn" disabled>
  				    	Save
  				    </Button>
    				}
    				<Button variant="secondary" onClick={closeAdd}>
    					Close
    				</Button>
    			</Modal.Footer>
    		</Form>	
    	</Modal>
	    
    	{/*Modal for editing a product*/}
      <Modal show={showEdit} fullscreen={false} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title>Update Product Information</Modal.Title>
    			</Modal.Header>

    			<Modal.Body>
	        	<Form.Group controlId="name" className="mb-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter Product Name" 
                value = {name}
                onChange={e => setName(e.target.value)}
                required />
            </Form.Group>

            <Form.Group controlId="description" className="mb-3">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
              	as="textarea"
              	rows={3}
                placeholder="Enter Product Description" 
                value = {description}
                onChange={e => setDescription(e.target.value)}
                required />
            </Form.Group>

            <Form.Group controlId="price" className="mb-3">
              <Form.Label>Product Price</Form.Label>
              <Form.Control 
                type="number" 
                min="0"
                placeholder="Enter Product Price" 
                value = {price}
                onChange={e => setPrice(e.target.value)}
                required />
            </Form.Group>

            <Form.Group controlId="stocks" className="mb-3">
              <Form.Label>Product Stocks</Form.Label>
              <Form.Control 
                type="number" 
                min="0"
                placeholder="Enter Product Stocks" 
                value = {stocks}
                onChange={e => setStocks(e.target.value)}
                required />
            </Form.Group>
    			</Modal.Body>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
  				    : 
  				    <Button variant="danger" type="submit" id="submitBtn" disabled>
  				    	Save
  				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>

    	{/*Modal for showing orders*/}
      <Modal show={showOrders} fullscreen={true} onHide={closeOrders}>

  			<Modal.Header closeButton>
  				<Modal.Title>Show Orders</Modal.Title>
  			</Modal.Header>

  			<Modal.Body>
  				{allOrders}
  			</Modal.Body>

  			<Modal.Footer>
  				<Button variant="secondary" onClick={closeOrders}>
  					Close
  				</Button>
  			</Modal.Footer>
    	</Modal>

    	{/*Modal for setting user role*/}
      <Modal show={showAdmin} fullscreen={false} onHide={closeAdmin}>

  			<Modal.Header closeButton>
  				<Modal.Title>Set User Role</Modal.Title>
  			</Modal.Header>

  			<Modal.Body>
					<Table striped bordered hover>
			      <thead>
			        <tr>
			          <th>First Name</th>
			          <th>Last Name</th>
			          <th>isAdmin</th>
			          <th>Actions</th>
			        </tr>
			      </thead>
			      <tbody>
		        	{allUsers}
			      </tbody>
			    </Table>
  			</Modal.Body>

  			<Modal.Footer>
  				<Button variant="secondary" onClick={closeAdmin}>
  					Close
  				</Button>
  			</Modal.Footer>
    	</Modal>
		</>
		:
		<Navigate to="/products" />
	)
}