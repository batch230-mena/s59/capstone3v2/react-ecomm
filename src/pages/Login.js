import {Container, Row, Col, Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

import Swal from "sweetalert2";

export default function Login(){

	const { user, setUser } = useContext(UserContext);

	// State hooks
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: 'POST',
				headers: { 'Content-type' : 'application/json'},
				body: JSON.stringify({
					email: email,
					password: password
				})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log("Check accessToken");
			console.log(data.access);

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Coffee Break!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})

			}

		})

		setEmail('');
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin,
				firstName: data.firstName
			})

			localStorage.setItem("userId", data._id);
			localStorage.setItem("userRole", data.isAdmin);
			localStorage.setItem("firstName", data.firstName);

			console.log("Login: ");
			console.log(user);
		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	return(
		(user.id !== null)
		? 
		<Navigate to="/products" />
		:
		<Container fluid>
			<h3>Login</h3>
			<Row>
				<Col className="col-md-3">
				</Col>
				<Col className="col-md-6 col-12">
						<Form onSubmit={(event) => loginUser(event)} >
				      
				      <Form.Group controlId="userEmail" className='pt-2 pb-2'>
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				          type="email" 
				          placeholder="Enter Email address"
				          value = {email} 
				          onChange = {event => setEmail(event.target.value)}
				          required />
				      </Form.Group>

				      <Form.Group controlId="password" className='pt-2 pb-2'>
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				          type="password" 
				          placeholder="Enter Password" 
				          value = {password} 
				          onChange = {event => setPassword(event.target.value)}
				          required />
				        <Form.Text className="text-muted">
				          Email address and password are required to log in.
				        </Form.Text>
				      </Form.Group>
				      
				      <div className='pt-3 pb-3'>    
				        { isActive ?
				        	<Button variant="danger" type="submit" id="submitBtn">
				        		Login
				        	</Button>
				        	: 
				        	<Button variant="danger" type="submit" id="submitBtn" disabled>
				        		Login
				        	</Button>
				        }
				      </div>
				    </Form>
				</Col>
				<Col className="col-md-3">
				</Col>
			</Row>
	  </Container>
	)
}
