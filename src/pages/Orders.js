import { Fragment } from 'react';
import { Navigate } from 'react-router-dom';
import { useEffect, useState, useContext } from 'react';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';
import LoggedInBanner from '../components/LoggedInBanner';

export default function Orders(){
	
	const {user} = useContext(UserContext);
	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setOrders(data.map((order, index) => {
				return(
					<OrderCard key={index} orderProp={order} />
				)
			}))
		})
	}, [])

	return (
		(user.isAdmin === true)
		?
		<Navigate to="/admin" />
		:
		<>
			<LoggedInBanner firstName={user.firstName} />
			<h3>Orders</h3>
			{orders}
		</>
	)
}
