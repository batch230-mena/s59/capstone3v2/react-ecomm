import {useState, useEffect, useContext} from 'react';
import {Container, Card, Form, Button, Row, Col} from "react-bootstrap";
import {useParams, useNavigate, Link} from "react-router-dom";
import UserContext from '../UserContext';
import Swal from "sweetalert2";

export default function ProductView(){
	const {user} = useContext(UserContext);
//	const {order} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	const [quantity, setQuantity] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);
		})

	}, [productId])

//----------------------------

	function createOrder(event){

	  event.preventDefault();

	  console.log("createOrder - user.id: " + user.id);

	  fetch(`${process.env.REACT_APP_API_URL}/users/checkOrder/`, {
	    method: 'POST',
	    headers: { 'Content-type' : 'application/json'},
	    body: JSON.stringify({
	      userId: user.id
	    })
	  })
	  .then(res => res.json())
	  .then(data => {
	    console.log(data);

	    if(data){
				buy(productId, user.id);
	    }
	    else{
	      createNewOrder(user.id);
	    }
	  })
	}

  const createNewOrder = (userId) => {

    fetch(`${process.env.REACT_APP_API_URL}/users/createOrder`, {
      method: 'POST',
      headers: { 'Content-type' : 'application/json'},
      body: JSON.stringify({
        totalAmount: 0,
        userId: userId
      })
    })
    .then(res => res.json())
    .then(data => {
    	console.log("createNewOrder");
      console.log(data);

      if(data){
				buy(productId, user.id)
      } 
      else{
        Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }
    })
  }

//----------------------------

	const buy = (productId, userId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity 
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log("Buy data");
			console.log(data);

			if(data){
				Swal.fire({
					title: "Checkout Successful",
					icon: "success",
					text: "You have successfully checkout the product."
				})

				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	return(
		<Container className="mt-5 mb-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>{stocks}</Card.Text>
							{
								(user.id !== null)
								?
								<Col sm={{ span: 2, offset: 5}}>
								<Form onSubmit={(event) => createOrder(event)}>
								  <Form.Group controlId="quantity" className="mt-3 mb-3">
								    <Form.Label>Quantity</Form.Label>
								    <Form.Control
								    	type="number" 
								    	min="1"
								    	max={stocks}
								      placeholder="Enter quantity" 
								      value = {quantity}
								      onChange = {event => setQuantity(event.target.value)}
								      required />
								  </Form.Group>
								  <Button variant="danger" size="lg" type="submit">Checkout</Button>
								</Form>
								</Col>
								:
									<Button as={Link} to="/login" variant="danger" size="lg">Login to checkout</Button>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}