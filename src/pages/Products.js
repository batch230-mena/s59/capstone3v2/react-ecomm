import { Fragment } from 'react';
import { Navigate } from 'react-router-dom';
import { Container, Row } from "react-bootstrap";
import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import LoggedInBanner from '../components/LoggedInBanner';

export default function Products(){
	
	const {user} = useContext(UserContext);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])

	return (
		(user.isAdmin)
		?
		<Navigate to="/admin" />
		:
		(user.id === null)
		?
		<Fragment>
			<h3>Products</h3>
			<Container className="mt-5 mb-5">
			  <Row>
					{products}
			  </Row>
			</Container>
		</Fragment>
		:	
		<Fragment>
			<LoggedInBanner firstName={user.firstName} />
			<h3>Products</h3>
			<Container className="mt-5 mb-5">
			  <Row>
					{products}
			  </Row>
			</Container>
		</Fragment>
	)
}
